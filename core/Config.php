<?php
/**
 * Created by PhpStorm.
 * User: mallmann
 * Date: 12/04/18
 * Time: 09:58
 */

namespace core;
use \PDO;

class Config
{
    private $dbname;
    private $host;
    private $user;
    private $pass;
    /**
     * @var \PDO
     */
    private $connection;

    function __construct()
    {
        $this->dbname = DBNAME;
        $this->host = DBHOST;
        $this->user= DBUSER;
        $this->pass = DBPASS;
    }

    function getConnection()
    {
        try{

            $this->connection = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->dbname, $this->user, $this->pass, [
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ]);

        }catch (\PDOException $e){
            echo 'Erro: ' . $e->getMessage();
        }

        return $this->connection;
    }

}